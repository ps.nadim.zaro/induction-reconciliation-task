package com.progressoft.jip8.samples;

import java.io.IOException;
import java.util.Scanner;

public class ReconApp {

    String sourceFile;
    String destFile;
    WriteRecon writer = new WriteRecon();
    ReadRecon reader = new ReadRecon();

    ReconApp() throws IOException {

        Scanner sourceScanner = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter source file location: ");
        String sourceName = sourceScanner.nextLine();
        this.sourceFile = sourceName;         // Read user input

        Scanner sourceTypeScanner = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter source file type: ");
        String typeName = sourceTypeScanner.nextLine();  // Read user input

        Scanner destScanner = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter target file location: ");
        String destName = destScanner.nextLine();
        this.destFile = destName;        // Read user input

        Scanner destTypeScanner = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter target file type: ");
        String destTypeName = destTypeScanner.nextLine();  // Read user input

        // TODO you are reading twice, here and inside compare
        if (typeName.equals("CSV")) {
            reader.readCSV(sourceName);
        } else {
            reader.readJSON(sourceName);
        }
        if (destTypeName.equals("JSON")) {
            reader.readJSON(destName);
        } else {
            reader.readCSV(destName);
        }
    }

    public void printData() throws IOException {
        writer.writeMissing();
        writer.writeMatched();
        writer.writeMismatched();
    }


    public static void main(String[] args) throws IOException {
        ReconApp ra = new ReconApp();
        ra.printData();
    }
}
