package com.progressoft.jip8.samples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Record {

    List<List<String>> list = new ArrayList<>();

    public List<List<String>> getRecord(String reference, String amount, String currencyCode, String purpose, String date){
        list.add(Arrays.asList(reference, amount, currencyCode, purpose, date));
        return list;
    }
}
