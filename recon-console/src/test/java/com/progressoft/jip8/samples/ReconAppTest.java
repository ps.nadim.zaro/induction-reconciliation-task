package com.progressoft.jip8.samples;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.stream.Stream;

public class ReconAppTest {

    @Test
    public void givenNullDataSourceCSV_whenInputtingFiles_thenThrowNullPointerException() {
        ReadRecon ra = new ReadRecon();
        Assertions.assertThrows(NullPointerException.class,
                () -> {
                    ra.readCSV(null);
                }, "Field is Null");
    }

    @Test
    public void givenNullDataSourceJSON_whenInputtingFiles_thenThrowNullPointerException() {
        ReadRecon ra = new ReadRecon();
        Assertions.assertThrows(NullPointerException.class,
                () -> {
                    ra.readJSON(null);
                }, "Field is Null");
    }

    @Test
    public void givenInvalidCSV_whenReadingFiles_thenThrowIllegalArgumentException() {
        ReadRecon ra = new ReadRecon();
        String InvalidSource = "/invalid/source";
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    ra.readCSV(InvalidSource);
                }, "Invalid File Reference");
    }

    @Test
    public void givenInvalidJSON_whenReadingFiles_thenThrowIllegalArgumentException() {
        ReadRecon ra = new ReadRecon();
        String InvalidSource = "/invalid/source";
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    ra.readJSON(InvalidSource);
                }, "Invalid File Reference");
    }

    @Test
    public void givenValidMatchWriter_whenWritingFile_thenReturnLocation() {
        CompareRecon ra = new CompareRecon();
        String listTest = ("[[TR-47884222201, 140, USD, donation, 2020-01-20], [TR-47884222203, 5000, JOD, , 2020-01-25], [TR-47884222206, 500, USD, general, 2020-02-10]]");
        String resultTest = ra.commonRecords.toString();
        Assertions.assertEquals(listTest, resultTest);
    }

    @Test
    public void givenValidMisMatchWriter_whenWritingFile_thenReturnLocation() {
        CompareRecon ra = new CompareRecon();
        String listTest = ("[[TR-47884222201, 140, USD, donation, 2020-01-20], [TR-47884222203, 5000, JOD, , 2020-01-25], [TR-47884222206, 500, USD, general, 2020-02-10]]");
        String resultTest = ra.commonRecords.toString();
        Assertions.assertEquals(listTest, resultTest);
    }

    @Test
    public void givenValidMissingWriter_whenWritingFile_thenReturnLocation() {
        CompareRecon ra = new CompareRecon();
        String listTest = ("[[TR-47884222201, 140, USD, donation, 2020-01-20], [TR-47884222203, 5000, JOD, , 2020-01-25], [TR-47884222206, 500, USD, general, 2020-02-10]]");
        String resultTest = ra.commonRecords.toString();
        Assertions.assertEquals(listTest, resultTest);
    }

    @Test
    public void givenValidMissingElements_whenComparingFiles_thenReturnElements() throws IOException {
        String location = "missing.csv";
        WriteRecon ra = new WriteRecon();
        String listTest = ("Writing Successful; Missing data at " + location);
        String resultTest = ra.writeMissing();
        Assertions.assertEquals(listTest, resultTest);
    }

    @Test
    public void givenValidMisMatchedElements_whenComparingFiles_thenReturnElements() throws IOException {
        String location = "mismatch.csv";
        WriteRecon ra = new WriteRecon();
        String listTest = ("Writing Successful; Mismatched data at " + location);
        String resultTest = ra.writeMismatched();
        Assertions.assertEquals(listTest, resultTest);
    }

    @Test
    public void givenValidCommonElements_whenComparingFiles_thenReturnElements() throws IOException {
        String location = "common.csv";
        WriteRecon ra = new WriteRecon();
        String listTest = ("Writing Successful; Matched data at " + location);
        String resultTest = ra.writeMatched();
        Assertions.assertEquals(listTest, resultTest);
    }

    @Test
    public void givenValidMismatchedElements_whenComparingFiles_thenReturnMismatchedElements() {
        CompareRecon ra = new CompareRecon();
        String listTest = ("[[TR-47884222202, 20, JOD, , 2020-01-22], [TR-47884222202, 30, JOD, donation, 2020-01-22], [TR-47884222205, 60, JOD, , 2020-02-02], [TR-47884222205, 60, JOD, , 2020-02-03]]");
        String resultTest = ra.mismatchRecords.toString();
        Assertions.assertEquals(listTest, resultTest);
    }

    @Test
    public void givenMissingElements_whenComparingFiles_thenReturnMissingElements() {
        CompareRecon ra = new CompareRecon();
        String listTest = ("[[TR-47884222217, 12000, JOD, salary, 2020-02-14], [TR-47884222245, 420, USD, loan, 2020-01-12]], [[TR-47884222204, 1200, JOD, donation, 2020-01-31]]");
        String resultTest = ra.missingRecords.toString();
        Assertions.assertEquals(listTest, resultTest);
    }

    @ParameterizedTest(name = "Happy Scenario [{index}] {arguments}")
    @ArgumentsSource(InputsProvider.class)
    public String givenValidFileReconciliation_whenComparingFiles_thenReturnSuccessfulFileData(CompareInputs inputs) {
        String dataSource = inputs.from;
        String dataTarget = inputs.to;
        BigDecimal exampleExpected = inputs.expected;
        if (dataSource.equals("140.00") && dataTarget.equals("140.00")) {
            return "TR-47884222201"; //matching transaction
        }
        if (dataSource.equals("12000.000") && dataTarget.equals("null")) {
            return "TR-47884222217"; //missing transaction
        }
        if (dataSource.equals("20.000") && dataTarget.equals("30.000")) {
            return "TR-47884222202"; //mismatched transaction
        } else if (Assertions.fail("Failed")) {
            return null;
        }
        CompareRecon reconTest = new CompareRecon();
        String result = reconTest.commonRecords.toString();
        Assertions.assertEquals(exampleExpected.toString(), result);
        return "Assertion Succesful";
    }

    static class InputsProvider implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            CompareInputs case1 = new CompareInputs();
            case1.amount = new BigDecimal("140.00");
            case1.expected = new BigDecimal("140.00");
            case1.from = "CSV";
            case1.to = "JSON";

            CompareInputs case2 = new CompareInputs();
            case2.amount = new BigDecimal("12000.000");
            case2.expected = new BigDecimal((char[]) null);
            case2.from = "CSV";
            case2.to = "JSON";

            CompareInputs case3 = new CompareInputs();
            case3.amount = new BigDecimal("20.000");
            case3.expected = new BigDecimal("30.000");
            case3.from = "CSV";
            case3.to = "JSON";

            return Stream.of(
                    Arguments.of(case1),
                    Arguments.of(case3),
                    Arguments.of(case2));
        }
    }

    static class CompareInputs {
        String from;
        String to;
        BigDecimal amount;
        BigDecimal expected;

        @Override
        public String toString() {
            return "{" +
                    "from='" + from + '\'' +
                    ", to='" + to + '\'' +
                    ", amount=" + amount +
                    ", expected=" + expected +
                    '}';
        }
    }
}