package com.progressoft.jip8.samples;

import java.text.ParseException;

public class NewParseException extends RuntimeException {
    public NewParseException(String message, ParseException exception) {
        super(message, exception);
    }
}
