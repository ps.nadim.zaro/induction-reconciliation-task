package com.progressoft.jip8.samples;

import java.io.FileWriter;
import java.io.IOException;

public class WriteRecon {

    CompareRecon compare = new CompareRecon();
    String homePath = System.getProperty("user.home");
    String src = homePath + "/bank-transactions.csv";
    String target = homePath + "/online-banking-transactions.json";

    public WriteRecon() throws IOException {
        compare.compareData(src, target);
    }

    public String writeMatched() throws IOException {
        String str = compare.commonRecords.toString();
        String location = "common.csv";
        // attach a file to FileWriter
        FileWriter fw = new FileWriter(homePath + "/common.csv");
        // into FileWriter
        for (int i = 0; i < str.length(); i++)
            fw.write(str.charAt(i));
        System.out.println("Writing Successful; Matched data at " + location);
        //close the file
        fw.close();
        return "Writing Successful; Matched data at " + location;
    }

    public String writeMismatched() throws IOException {
        String str = compare.mismatchRecords.toString();
        String location = "mismatch.csv";
        // attach a file to FileWriter
        FileWriter fw = new FileWriter(homePath + "/mismatch.csv");
        // into FileWriter
        for (int i = 0; i < str.length(); i++)
            fw.write(str.charAt(i));
        System.out.println("Writing Successful; Mismatched data at " + location);
        //close the file
        fw.close();
        return "Writing Successful; Mismatched data at " + location;
    }

    public String writeMissing() throws IOException {
        String str = compare.missingRecords.toString();
        String location = "missing.csv";
        // attach a file to FileWriter
        FileWriter fw = new FileWriter(homePath + "/missing.csv");
        // into FileWriter
        for (int i = 0; i < str.length(); i++)
            fw.write(str.charAt(i));
        System.out.println("Writing Successful; Missing data at " + location);
        //close the file
        fw.close();
        return "Writing Successful; Missing data at " + location;
    }
}
