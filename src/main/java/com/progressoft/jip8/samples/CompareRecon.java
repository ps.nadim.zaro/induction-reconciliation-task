package com.progressoft.jip8.samples;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CompareRecon {

    ReadRecon read = new ReadRecon();

    public CompareRecon() {
    }

    List<String> missingRecords = new ArrayList<>();
    List<String> commonRecords = new ArrayList<>();
    List<String> mismatchRecords = new ArrayList<>();

    public void compareData(String source, String dest) throws IOException {
        List<List<String>> sourceRecords = read.readCSV(source);
        List<List<String>> removeIntersection = read.readCSV(source);
        // TODO you always assume it is going to be a json
        List<List<String>> destRecords = read.readJSON(dest);
        List<List<String>> destRecordsClone = new ArrayList<>(destRecords);
        List<List<String>> sameIdRecords = new ArrayList<>();

        removeIntersection.retainAll(destRecords);
        //remove all elements from second list
        destRecords.removeAll(sourceRecords);
        sourceRecords.removeAll(destRecordsClone);

        for (List<String> first : sourceRecords) {
            String oneRef = first.get(0);
            for (List<String> second : destRecords) {
                String twoRef = second.get(0);
                if (oneRef.equals(twoRef)) {
                    sameIdRecords.add(first);
                    sameIdRecords.add(second);
                }
            }
        }

        destRecords.removeAll(sameIdRecords);
        sourceRecords.removeAll(sameIdRecords);
        // TODO the result format should be a CSV
        missingRecords.add(destRecords.toString());
        missingRecords.add(sourceRecords.toString());
        mismatchRecords.add(sameIdRecords.toString());
        commonRecords.add(removeIntersection.toString());

    }


}
