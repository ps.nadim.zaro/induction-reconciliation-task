package com.progressoft.jip8.core;

import java.io.IOException;

public class FileException extends RuntimeException {
    public FileException(String message, IOException exception) {
        super(message,exception);
    }
}
