package com.progressoft.jip8.core;

import java.io.IOException;

public class ReconApp {

    ReadRecon reader = new ReadRecon();
    WriteRecon writer = new WriteRecon();

    public ReconApp(String sourceFile, String destFile, String sourceType, String destType) throws IOException {

        if (sourceType.equals("CSV")) {
            reader.readCSV(sourceFile);
        } else {
            reader.readJSON(sourceFile);
        }
        if (destType.equals("JSON")) {
            reader.readJSON(destFile);
        } else {
            reader.readCSV(destFile);
        }
    }

    public void printData() throws IOException {
        writer.writeMissing();
        writer.writeMatched();
        writer.writeMismatched();
    }
}
