package com.progressoft.jip8.core;

import java.text.ParseException;

public class NewParseException extends RuntimeException {
    public NewParseException(String message, ParseException exception) {
        super(message, exception);
    }
}
