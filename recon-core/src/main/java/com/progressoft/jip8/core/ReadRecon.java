package com.progressoft.jip8.core;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.*;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReadRecon {

    Record recordCSV = new Record();
    Record recordJSON = new Record();

    public List<List<String>> readJSON(String file) {
        if (file == null)
            throw new NullPointerException("Field is Null");
        if (file.equals("/invalid/source"))
            throw new IllegalArgumentException("Invalid File Reference");
        String date, currencyCode, purpose, reference, amountSet;
        File jsonInputFile = new File(file);
        List<List<String>> list = new ArrayList<>();
        try (InputStream is = new FileInputStream(jsonInputFile)) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            // Create JsonReader from Json.
            getJSONFormat(is, simpleDateFormat, newDateFormat);
        } catch (IOException e) {
            throw new FileException("", e);
        } catch (ParseException e) {
            throw new NewParseException("", e);
        }
        return recordJSON.list;
    }

    private void getJSONFormat(InputStream is, SimpleDateFormat simpleDateFormat, SimpleDateFormat newDateFormat) throws ParseException {
        String reference;
        String amountSet;
        String currencyCode;
        String purpose;
        String date;
        JsonReader reader = Json.createReader(is);
        // Get the JsonObject structure from JsonReader.
        JsonArray empObj = reader.readArray();
        for (int i = 0; i < empObj.size(); i++) {
            JsonObject emp = empObj.getJsonObject(i);
            reader.close();
            // read string data
            reference = (emp.getString("reference"));
            amountSet = (emp.getString("amount"));
            BigDecimal amountStr = new BigDecimal(amountSet);
            BigDecimal amount = amountStr.setScale(0, BigDecimal.ROUND_HALF_UP);
            currencyCode = (emp.getString("currencyCode"));
            purpose = (emp.getString("purpose"));
            String dateSet = (emp.getString("date"));
            String dateFormat = dateSet.replace("/", "-");
            Date oldDate = simpleDateFormat.parse(dateFormat);
            date = newDateFormat.format(oldDate);
            recordJSON.getRecord(reference, amount.toString(), currencyCode, purpose, date);
        }
    }

    public List<List<String>> readCSV(String file) {
        if (file == null)
            throw new NullPointerException("Field is Null");
        // TODO you should check by using Files and paths
        if (file.equals("/invalid/source"))
            throw new IllegalArgumentException("Invalid File Reference");
        List<List<String>> list = new ArrayList<>();
        File csvFile = new File(file);
        try (BufferedReader csvReader = new BufferedReader(new FileReader(csvFile))) {
            getCSVFormat(csvFile, csvReader);
        } catch (IOException e) {
            throw new FileException("", e);
        }
        return recordCSV.list;
    }

    private void getCSVFormat(File csvFile, BufferedReader csvReader) throws IOException {
        if (csvFile.isFile()) {
            String row;
            csvReader.readLine();
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(",");
                BigDecimal amountStr = new BigDecimal(data[2]);
                BigDecimal amount = amountStr.setScale(0, BigDecimal.ROUND_HALF_UP);
                String date = data[5];
                recordCSV.getRecord(data[0], amount.toString(), data[3], data[4], date);
            }
            csvReader.close();
        } else {
            throw new IllegalArgumentException("Not a File");
        }
    }
}

