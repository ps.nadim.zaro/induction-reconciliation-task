package com.progressoft.jip8.core;

import java.io.FileWriter;
import java.io.IOException;

public class WriteRecon {

    CompareRecon compare = new CompareRecon();
    String homePath = System.getProperty("user.home");
    String src = homePath + "/induction-reconciliation-task/recon-webapp/input-folder/bank-transactions.csv";
    String target = homePath + "/induction-reconciliation-task/recon-webapp/input-folder/online-banking-transactions.json";

    public WriteRecon() throws IOException {
        compare.compareData(src, target);
    }

    public String writeMatched() throws IOException {
        String str = compare.commonRecords.toString();
        String location = "common.csv";
        // attach a file to FileWriter
        FileWriter fw = new FileWriter("/home/user/induction-reconciliation-task/recon-webapp/main-web/src/main/webapp/Data-Files/matched.csv");
        // into FileWriter
        for (int i = 0; i < str.length(); i++)
            fw.write(str.charAt(i));
        System.out.println("Writing Successful; Matched data at " + location);
        //close the file
        fw.close();
        return "Writing Successful; Matched data at " + location;
    }

    public String writeMismatched() throws IOException {
        String str = compare.mismatchRecords.toString();
        String location = "mismatch.csv";
        // attach a file to FileWriter
        FileWriter fw = new FileWriter("/home/user/induction-reconciliation-task/recon-webapp/main-web/src/main/webapp/Data-Files/mismatch.csv");
        // into FileWriter
        for (int i = 0; i < str.length(); i++)
            fw.write(str.charAt(i));
        System.out.println("Writing Successful; Mismatched data at " + location);
        //close the file
        fw.close();
        return "Writing Successful; Mismatched data at " + location;
    }

    public String writeMissing() throws IOException {
        String str = compare.missingRecords.toString();
        String location = "missing.csv";
        // attach a file to FileWriter
        FileWriter fw = new FileWriter("/home/user/induction-reconciliation-task/recon-webapp/main-web/src/main/webapp/Data-Files/missing.csv");
        // into FileWriter
        for (int i = 0; i < str.length(); i++)
            fw.write(str.charAt(i));
        System.out.println("Writing Successful; Missing data at " + location);
        //close the file
        fw.close();
        return "Writing Successful; Missing data at " + location;
    }
}
