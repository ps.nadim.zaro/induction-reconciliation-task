package com.progressoft.jip8.webcore;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

@WebServlet(urlPatterns = "/UploadFile")
public class FileUploadServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (Objects.isNull(req.getSession().getAttribute("sourcePath"))) {
            resp.sendRedirect(req.getContextPath() + "/source-file-form");
            return;
        }
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/target-upload-form.jsp");
        requestDispatcher.forward(req, resp);
        req.getSession().removeAttribute("error-source");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
        List<FileItem> files;
        Path filePath;
        int i = 0;
        try {
            files = servletFileUpload.parseRequest(req);
            for (FileItem item : files) {
                if (isFile(item)) {
                    Path path = Paths.get(Paths.get(".").toAbsolutePath().normalize() + "/input-folder/");
                    Files.createDirectories(path);
                    filePath = Paths.get(path + "/" + item.getName());
                    req.getSession().setAttribute(String.valueOf(i), filePath);
                    item.write(new File(filePath.toString()));
                    i++;
                }
            }
        } catch (Exception e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }
        resp.sendRedirect("result.jsp");
    }

    private boolean isFile(FileItem item) {
        return !item.isFormField();
    }
}