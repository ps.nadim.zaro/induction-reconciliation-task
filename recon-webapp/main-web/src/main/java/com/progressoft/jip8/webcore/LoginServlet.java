
package com.progressoft.jip8.webcore;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Login")
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String uname = request.getParameter("uname");
        String pass = request.getParameter("pass");

        if (uname.equals("admin") && pass.equals("P@ssw0rd")) {
            response.sendRedirect("homePage.jsp");
            return;
        }

        response.sendRedirect("index.jsp");
    }
}