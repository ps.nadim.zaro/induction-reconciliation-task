
package com.progressoft.jip8.webcore;

import com.progressoft.jip8.core.*;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/HomePage")
public class TransferFileServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String sourceType = request.getParameter("sourceFileType");
        String targetType = request.getParameter("targetFileType");

        String sourceDest = request.getParameter("sourceFile");
        String targetDest = request.getParameter("targetFile");

        ReconApp ra = new ReconApp(sourceDest, targetDest, sourceType, targetType);
        ra.printData();

        response.sendRedirect("result.jsp");
    }
}
