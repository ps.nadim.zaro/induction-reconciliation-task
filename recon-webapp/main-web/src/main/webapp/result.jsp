
<!DOCTYPE html>
<html>
 <head>
  <title>Reconciled Data</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 </head>
 <body>
  <div class="container">
   <div class="header">
    <h1 align="center">Reconciled Data</h1>

     <link rel="stylesheet" type="text/css" href="Header.css">

    <br />
    <div align="center">
     <button type="button" name="load_matched" id="load_matched" class="btn btn-info">Load Matched</button>
     <button type="button" name="load_mismatch" id="load_mismatch" class="btn btn-info">Load Mismatched</button>
     <button type="button" name="load_missing" id="load_missing" class="btn btn-info">Load Missing</button>
    </div>
    <br />
    <div id="processed_table">
    </div>
   </div>
  </div>
 </body>
</html>

<script>
$(document).ready(function(){
 $('#load_matched').click(function(){
  $.ajax({
   url:"Data-Files/matched.csv",
   dataType:"text",
   success:function(data)
   {
    var matched_data = data.split(/\r?\n|\r/);
    var table_data = '<table class="table table-bordered table-striped">';
    for(var count = 0; count<matched_data.length; count++)
    {
     var cell_data = matched_data[count].split(",");
     table_data += '<tr>';
     for(var cell_count=0; cell_count<cell_data.length; cell_count++)
     {
      if(count === 0)
      {
       table_data += '<th>'+cell_data[cell_count]+'</th>';
      }
      else
      {
       table_data += '<td>'+cell_data[cell_count]+'</td>';
      }
     }
     table_data += '</tr>';
    }
    table_data += '</table>';
    $('#processed_table').html(table_data);
    $("tr:even").css("background-color", "#000000");
   }
  });
 });

});
</script>

<script>
$(document).ready(function(){
 $('#load_mismatch').click(function(){
  $.ajax({
   url:"Data-Files/mismatch.csv",
   dataType:"text",
   success:function(data)
   {
    var mismatch_data = data.split(/\r?\n|\r/);
    var table_data = '<table class="table table-bordered table-striped">';
    for(var count = 0; count<mismatch_data.length; count++)
    {
     var cell_data = mismatch_data[count].split(",");
     table_data += '<tr>';
     for(var cell_count=0; cell_count<cell_data.length; cell_count++)
     {
      if(count === 0)
      {
       table_data += '<th>'+cell_data[cell_count]+'</th>';
      }
      else
      {
       table_data += '<td>'+cell_data[cell_count]+'</td>';
      }
     }
     table_data += '</tr>';
    }
    table_data += '</table>';
    $('#processed_table').html(table_data);
    $("tr:even").css("background-color", "#000000");
   }
  });
 });

});
</script>

<script>
$(document).ready(function(){
 $('#load_missing').click(function(){
  $.ajax({
   url:"Data-Files/missing.csv",
   dataType:"text",
   success:function(data)
   {
    var missing_data = data.split(/\r?\n|\r/);
    var table_data = '<table class="table table-bordered table-striped">';
    for(var count = 0; count<missing_data.length; count++)
    {
     var cell_data = missing_data[count].split(",");
     table_data += '<tr>';
     for(var cell_count=0; cell_count<cell_data.length; cell_count++)
     {
      if(count === 0)
      {
       table_data += '<th>'+cell_data[cell_count]+'</th>';
      }
      else
      {
       table_data += '<td>'+cell_data[cell_count]+'</td>';
      }
     }
     table_data += '</tr>';
    }
    table_data += '</table>';
    $('#processed_table').html(table_data);
    $("tr:even").css("background-color", "#000000");
   }

  });
 });

});
</script>

<div align="center">
<a href="Data-Files" download>
  <img src="download-button-gif-4.gif" alt="download" width="250" height="200">
</a>
</div>



