package com.progressoft.jip8.webcore;


import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;


public class LoginServletTest {

    @Test
    public void givenInvalidLogin_whenLoginAttempt_thenRedirectToLoginPage() {
        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
        HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);
        when(httpServletRequest.getParameter("uname")).thenReturn("admin");
        when(httpServletRequest.getParameter("pass")).thenReturn("P@ssw0rd");
        LoginServlet loginFilter = new LoginServlet();
        try {
            loginFilter.doPost(httpServletRequest, httpServletResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            verify(httpServletResponse).sendRedirect("homePage.jsp");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
