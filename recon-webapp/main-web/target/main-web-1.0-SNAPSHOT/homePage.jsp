<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Reconciliation System</title>
</head>

<link rel="stylesheet" type="text/css" href="Header.css">

<div class="header">
    <h1>Reconciliation System</h1>
    <p>ProgressSoft Corporation</p>
</div>

<body>
<form action = "${pageContext.request.contextPath}/UploadFile" method="post" enctype="multipart/form-data">
    Select Source File:
    <input type="file" name="sourceFile" id="sourceFile">
<br/>
<label for="sourceFile">Choose Source File Type:</label>
<select name="sourceFileType" id="sourceFileType">
    <option value="csv">CSV</option>
    <option value="json">JSON</option>
    <option value="txt">TXT</option>
    <option value="excel">EXCEL</option>
</select>
<br/>
    Select Target File:
    <input type="file" name="targetFile" id="targetFile">
<br/>
<label for="targetFile">Choose Target File Type:</label>
<select name="targetFileType" id="targetFileType">
    <option value="csv">CSV</option>
    <option value="json">JSON</option>
    <option value="txt">TXT</option>
    <option value="excel">EXCEL</option>
</select>
<hr>
<input type="submit" value="Submit" name="submit">
</form>
</body>

</html>

